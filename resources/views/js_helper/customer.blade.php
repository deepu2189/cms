<script>

    $(".toglesearch").click(function(){
        $(".advance-search").removeClass("hide");
        $(".normal-search").addClass("hide");
        $(".nrml-btn").removeClass("hide");
        $(this).addClass("hide");
    });
    $(".nrml-btn").click(function(){

        $(".advance-search").addClass("hide");
        $(".normal-search").removeClass("hide");
        $(".nrml-btn").addClass("hide");
        $(".toglesearch").removeClass("hide");
    });


        {{--$('#laravel_datatable').DataTable({--}}
            {{--processing: true,--}}
            {{--serverSide: true,--}}
            {{--ajax: "{{ route('customers.index') }}",--}}
            {{--columns: [--}}
                {{--{ },--}}
                {{--{ },--}}
                {{--{ },--}}
                {{--{}--}}
            {{--]--}}
        {{--});--}}

        $( ".normal-search" ).keyup(function() {
            let data = $(this).val();
            $.ajax({
                type:'POST',
                url:'/search',
                data:{
                    '_token':'<?php echo csrf_token() ?>',
                    name:data
                },
                success:function(data) {
                    $('#laravel_datatable').html('');
                    $('#laravel_datatable').append(data);
                }
            });
        });

</script>
