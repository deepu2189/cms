@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading mb-5"><span>Customer Listing</span>
                        <div class="btn-and serach">
                            <div class="search-field">
                                <form><input class="normal-search" type="text" value="" placeholder="Search.."> <input
                                            class="advance-search hide" type="text" placeholder="Advance  Search..">
                                    <span class="toglesearch">Advance Search</span> <span class="nrml-btn hide">Normal Search</span>
                                </form>
                            </div>

                            <a href="{{ route('customer.create') }}" class="btn btn-primary float-right">New
                                Customer</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table id="laravel_datatable" class="table">
                            <tr>
                                <th>Sr No.</th>
                                <th>Name</th>
                                <th>Skill</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            @foreach( $users as $user )
                                <tr>
                                    <td>{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration }}</td>
                                    <td>{{ $user->first_name .' '. $user->last_name }}</td>
                                    <td>{{ $user->skill }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td>
                                        <a class="btn btn-primary"
                                           href="{{ route( 'customers.edit', $user->id ) }}">Edit</a>
                                        <a class="btn btn-primary btn-danger"
                                           href="{{ route( 'customers.destroy', $user->id ) }}" onclick="event.preventDefault();
                                                     document.getElementById( 'delete-user' ).submit();">Delete</a>
                                        <form id="delete-user"
                                              action="{{ route( 'customers.destroy', ['customer'=>$user->id] ) }}"
                                              method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            @if(count($users) == 0)
                                <tr>
                                    <td>No Record Found</td>
                                </tr>
                            @endif
                        </table>
                            @if(count($users) > 0)
                                {{ $users->render() }}
                            @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    @include('js_helper.customer')
@endsection
