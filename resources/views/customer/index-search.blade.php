

                            <tr>
                                <th>Sr No.</th>
                                <th>Name</th>
                                <th>Skill</th>
                                <th>Address</th>
                                <th>Action</th>
                            </tr>
                            @foreach( $users as $user )
                                <tr>
                                    <td>{{ (($users->currentPage() - 1 ) * $users->perPage() ) + $loop->iteration }}</td>
                                    <td>{{ $user->first_name .' '. $user->last_name }}</td>
                                    <td>{{ $user->skill }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td>
                                        <a class="btn btn-primary"
                                           href="{{ route( 'customers.edit', $user->id ) }}">Edit</a>
                                        <a class="btn btn-primary btn-danger"
                                           href="{{ route( 'customers.destroy', $user->id ) }}" onclick="event.preventDefault();
                                                     document.getElementById( 'delete-user' ).submit();">Delete</a>
                                        <form id="delete-user"
                                              action="{{ route( 'customers.destroy', ['customer'=>$user->id] ) }}"
                                              method="POST" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            @if(count($users) == 0)
                                <tr>
                                    <td>No Record Found</td>
                                </tr>
                            @endif

                        @if(count($users) > 0)
                            {{ $users->render() }}
                        @endif

