<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * Author Deepak
 * Customer Routes
 */
Route::resource('customers' , 'CustomerController');

Route::post('search' , 'CustomerController@search')->name('search');

Route::get('/customer/create', 'CustomerController@createCustomer')->name('customer.create');

