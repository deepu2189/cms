<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Mockery\Exception;

class CustomerController extends Controller
{
    /**
     * Author Deepak Thakur
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.index', ['users' => User::paginate(Config::get('app.pagination_limit'))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRegisterRequest $request, User $user)
    {
        try {
            DB::beginTransaction();
            if ($request->persist($user)) {
                DB::commit();
            }
            return redirect()->route('customers.index');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('customer.edit', ['user' => User::find($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRegisterRequest $request, User $customer)
    {
        try {
            DB::beginTransaction();
            if ($request->persist($customer)) {
                DB::commit();
            }
            return redirect()->route('customers.index');

        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $customer)
    {
        try {
            DB::beginTransaction();
            if ($customer->delete()) {
                DB::commit();
            }
            return redirect()->route('customers.index')->with('message', 'User Successfully Deleted');
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    public function createCustomer()
    {
        return view('customer.create');
    }

    /**
     * Author Deepak Thakur
     * @return mixed
     */
    public function search()
    {

        $this->User = new User();
        $data = request('name');
        if (request()->has('name') && request()->get('name') !== null) {
            $query = $this->User->newQuery()
                ->orderBy('last_name', 'Asc')
                ->orderBy('first_name', 'Asc');


            $query->whereRaw('LCASE(`first_name`) LIKE ?', ["%" . trim(strtolower($data)) . "%"])
                ->orWhereRaw('LCASE(`last_name`) LIKE ?', ["%" . trim(strtolower($data)) . "%"])
                ->orWhereRaw('LCASE(`skill`) LIKE ?', ["%" . trim(strtolower($data)) . "%"]);


            if (request()->get('pagination', "true") == "true") {
                $paginator = $query->paginate(
                    request()->get('limit', \Config::get('app.pagination_limit')),
                    ['*'],
                    'page',
                    request()->get('page', 1)
                );

                return view('customer.index-search', ['users' => $paginator]);

            }

        }
        return view('customer.index-search', [User::paginate(Config::get('app.pagination_limit'))]);

    }
}
