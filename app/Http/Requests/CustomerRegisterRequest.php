<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\File;


class CustomerRegisterRequest extends FormRequest
{
    /**
     * Author Deepak Thakur
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|alpha',
            'last_name' => 'sometimes',
            'skill' => 'required',
            'cv' => 'required|mimes:zip,pdf|max:2048',
            'address' => 'required',
        ];
    }

    /**
     * Author Deepak Thakur
     * @param $user
     */
    public function persist($customer)
    {

        $validated = $this->validated();
        $validated['cv'] = $this->upload($validated['cv']);

        if ($customer->id) {

            $customer->update($validated);
            return true;

        } else {

            $customer->create($validated);
            return true;
        }

    }

    public function upload($fileData = null)
    {

        $uniqueFileName = time() . rand(0, 9) . '.' . $fileData->getClientOriginalExtension();
        $destinationPath = public_path('cv');
        $fileData->move($destinationPath, $uniqueFileName);

        return $uniqueFileName;
    }

    public function messages()
    {

        return [
            'cv.mimes' => 'The cv must be a file of type: Doc or Pdf only.',
        ];
    }
}
